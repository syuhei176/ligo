open Types

val assert_value_eq : ( expression * expression ) -> unit option
val is_value_eq : ( expression * expression ) -> bool
