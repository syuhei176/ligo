(* The content of types.ml has been split into Ast which contains only
   type declarations, and Types_utils which contains some alias
   declarations and other definitions used by the fold generator. *)
include Ast
include SolverDBPlugins.Dep_cycle(Typer_errors)
