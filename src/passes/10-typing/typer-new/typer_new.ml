include Typer
module Heuristic_tc_fundep = Heuristic_tc_fundep (* must be redeclared here to access it from the tests *)
module Heuristic_break_ctor = Heuristic_break_ctor
module Heuristic_access_label = Heuristic_access_label
module Solver_should_be_generated = Solver_should_be_generated
module Compare_renaming = Compare_renaming
module Typecheck = Typecheck
